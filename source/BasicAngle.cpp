#include <iostream>
#include <math.h>
#include "BasicAngle.hpp"
#include "constants.hpp"

using std::cout;
using std::endl;
using std::ostream;
using std::ifstream;

BasicAngle::BasicAngle()
{
    _value = 0;
}

BasicAngle::BasicAngle(double value)
{
    _value = fmod(value, 2*PI);
    if (_value > PI)
        _value = _value - 2*PI;
    if (_value < -PI)
        _value = _value + 2*PI;
}

ostream &operator<<(ostream &os, const BasicAngle &a)
{
    os << a._value;
    return os;
}

double BasicAngle::get_radian() const
{
    return _value;
}

void BasicAngle::set_radian(double value)
{
    _value = fmod(value, 2*PI);
    if (_value > PI)
        _value = _value - 2*PI;
    if (_value < -PI)
        _value = _value + 2*PI;
}
