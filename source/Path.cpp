#include <iostream>
#include <fstream>
#include <stdio.h>
#include "Path.hpp"
#include "Point.hpp"

#define MIN(x,y) {x<y?x:y}
#define MAX(x,y) {x>y?x:y}

using std::cout;
using std::endl;
using std::ostream;
using std::ifstream;

Path::Path(const char *filename, TypeCoord type_coord)
{
    ifstream file(filename);

    while (!file.eof())
        if (type_coord == CARTESIAN) {
            double x;
            double y;
            file >> x >> y;
            Point p(x, y);
            _points.push_back(p);
        }
        else {
            double r;
            double a_value;
            file >> r >> a_value;
            Angle a(a_value);
            Point p(r, a);
            _points.push_back(p);
        }
}

void Path::display(TypeCoord type_coord) const
{
    vector<Point>::const_iterator p;
    for (p = _points.begin(); p != _points.end(); p++)
        if (type_coord == CARTESIAN) {
            p->display();
            cout << ") -> (";
        }
        else {
            p->display(POLAR);
            cout << ") -> (";
        }
}

double Path::length() const
{
    double length = 0;

    vector<Point>::const_iterator p;
    for (p = _points.begin(); p != _points.end(); p++) {
        auto p_next = std::next(p, 1);
        if (p_next == _points.end())
            break;
        length += p->distance(*p_next);
    }
    return length;
}

void Path::translation(const Point &new_start)
{
    vector<Point>::iterator p;
    Depl translation_vector = new_start - _points.at(0);
    for (p = _points.begin(); p != _points.end(); p++)
        *p += translation_vector;
}

Rectangle Path::rectangle() const
{
    double left;
    double right;
    double top;
    double bottom;

    bool initialized = false;

    vector<Point>::const_iterator p;
    for (p = _points.begin(); p != _points.end(); p++)
        if (!initialized) {
            left = p->get_X();
            right = p->get_X();
            top = p->get_Y();
            bottom = p->get_Y();
        }
        else {
            left = MIN(left, p->get_X());
            right = MAX(left, p->get_X());
            top = MAX(left, p->get_Y());
            bottom = MIN(left, p->get_Y());
        }

    Rectangle rectangle;
    rectangle.left = left;
    rectangle.right = right;
    rectangle.top = top;
    rectangle.bottom = bottom;

    return rectangle;
}
