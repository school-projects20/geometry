#pragma once

#include "BasicAngle.hpp"

enum Unit {RADIAN, DEGREE};

class Angle: public BasicAngle
{
    public:
        Angle();
        Angle(double value, Unit unit=RADIAN);
        double get_degree() const;
        void set_degree(double v);
        void display(Unit unit=RADIAN) const;
        Angle operator+(const Angle &a2) const;
        Angle operator*(double multiplier) const;
        double sine() const;
        double cosine() const;
        double tangent() const;
        static Angle arcsin(double x);
        static Angle arccos(double x);
        static Angle arctan(double x);
        static Angle arctan2(double y, double x);
};
