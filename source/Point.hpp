#pragma once

#include "Angle.hpp"

enum TypeCoord {CARTESIAN, POLAR};

struct Depl {
    double dx;
    double dy;
};

class Point
{
    private:
        double _x;
        double _y;
    public:
        Point();
        Point(double x, double y);
        Point(double r, Angle &a);
        double get_X() const;
        double get_Y() const;
        double get_R() const;
        Angle get_T() const;
        void set_X(double x);
        void set_Y(double y);
        void set_R(double r);
        void set_T(Angle &T);
        void display(TypeCoord type_coord=CARTESIAN) const;
        Point operator+(Depl d) const;
        Point &operator+=(Depl d);
        Depl operator-(const Point &p) const;
        double distance(const Point &p) const;
};
