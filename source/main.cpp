#include <iostream>

#include "BasicAngle.hpp"
#include "Angle.hpp"
#include "Point.hpp"
#include "Path.hpp"

using std::cout;
using std::endl;
using std::ostream;
using std::ifstream;

int main()
{
    Angle a1(4.18, RADIAN);
    Angle a2(60.5, DEGREE);
    Angle a3(0.94);

    cout << a1 << " & " << a2 << " & " << a3 << endl;
    cout << a2 << " & " << a2.get_degree() << endl;

    a2.display();
    cout << endl;
    a2.display(DEGREE);
    cout << endl;

    Angle a4 = a1 + a3;
    cout << "a4: " << a4 << endl;
    a4 = a4 * 0.5;
    cout << "a4: " << a4 << endl;

    Point p1(1, 1);
    p1.display(POLAR);
    cout << endl;
    Depl d;
    d.dx = 2;
    d.dy = -1;
    Point p2 = p1 + d;
    p2.display();
    cout << endl;

    Path c1("data/chemin.txt", CARTESIAN);
    c1.display(CARTESIAN);
    cout << endl;
    Point new_start(10, 0);
    c1.translation(new_start);
    c1.display(CARTESIAN);
    cout << endl;
    cout << c1.length() << endl;
    return 0;
}
