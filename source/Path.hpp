#pragma once

#include <vector>
#include "Point.hpp"

using std::vector;

struct Rectangle
{
    double left;
    double right;
    double top;
    double bottom;
};

class Path
{
    private:
        vector<Point> _points;

    public:
        Path(const char *filename, TypeCoord type_coord);
        void display(TypeCoord type_coord) const;
        double length() const;
        void translation(const Point &new_start);
        Rectangle rectangle() const;
};
