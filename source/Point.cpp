#include <iostream>
#include <math.h>
#include "Point.hpp"
#include "Angle.hpp"

using std::cout;
using std::endl;
using std::ostream;
using std::ifstream;

Point::Point()
{
    _x = 0;
    _y = 0;
}

Point::Point(double x, double y)
{
    _x = x;
    _y = y;
}

Point::Point(double r, Angle &a)
{
    _x = r * a.cosine();
    _y = r * a.sine();
}

double Point::get_X() const
{
    return _x;
}

double Point::get_Y() const
{
    return _y;
}

double Point::get_R() const
{
    return sqrt(_x*_x + _y*_y);
}

Angle Point::get_T() const
{
    Angle result = Angle::arctan2(_x, _y);
    return result;
}

void Point::set_X(double x)
{
    _x = x;
}

void Point::set_Y(double y)
{
    _y = y;
}

void Point::set_R(double r)
{
    Angle _a = this->get_T();
    _x = r * _a.cosine();
    _y = r * _a.sine();
}

void Point::set_T(Angle &a)
{
    double _r = this->get_R();
    _x = _r * a.cosine();
    _y = _r * a.sine();
}

void Point::display(TypeCoord type_coord) const
{
    if (type_coord == CARTESIAN)
        cout << "x: " << _x << ", y: " << _y;
    else
        cout << "r: " << this->get_R() << ", a: " << this->get_T();
}

Point Point::operator+(Depl d) const
{
    Point result(_x + d.dx, _y + d.dy);
    return result;
}

Point& Point::operator+=(Depl d)
{
    _x += d.dx;
    _y += d.dy;
    return *this;
}

Depl Point::operator-(const Point &p) const
{
    Depl d;
    d.dx = _x - p._x;
    d.dy = _y - p._y;
    return d;
}

double Point::distance(const Point &p) const
{
    return sqrt((_x-p._x)*(_x-p._x) + (_y-p._y)*(_y-p._y));
}
