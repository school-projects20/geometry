#pragma once

#include <iostream>

using std::cout;
using std::endl;
using std::ostream;
using std::ifstream;

class BasicAngle
{
    private:
        double _value;
    public:
        BasicAngle();
        BasicAngle(double value);
        friend ostream &operator<<(ostream &os, const BasicAngle &a);
        double get_radian() const;
        void set_radian(double v);
};
