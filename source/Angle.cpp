#include <iostream>
#include <math.h>
#include "Angle.hpp"
#include "BasicAngle.hpp"
#include "constants.hpp"

using std::cout;
using std::endl;
using std::ostream;
using std::ifstream;

Angle::Angle()
    :BasicAngle()
{
}

Angle::Angle(double value, Unit unite)
{
    if (unite == RADIAN)
        this->set_radian(value);
    else
        this->set_radian(value * (2*PI) / 360);
}

double Angle::get_degree() const
{
    return (this->get_radian() * 360) / (2*PI);
}

void Angle::set_degree(double value)
{
    this->set_radian(value * (2*PI) / 360);
}

void Angle::display(Unit unite) const
{
    if (unite == RADIAN)
        cout << this->get_radian();
    else
        cout << this->get_radian() * 360 / (2*PI);
}

Angle Angle::operator+(const Angle &a2) const
{
    Angle result(this->get_radian() + a2.get_radian());
    return result;
}

Angle Angle::operator*(double multiplier) const
{
    Angle result(this->get_radian() * multiplier);
    return result;
}

double Angle::sine() const
{
    return sin(this->get_radian());
}

double Angle::cosine() const
{
    return cos(this->get_radian());
}

double Angle::tangent() const
{
    return tan(this->get_radian());
}

Angle Angle::arcsin(double x)
{
    Angle result(asin(x));
    return result;
}

Angle Angle::arccos(double x)
{
    Angle result(acos(x));
    return result;
}

Angle Angle::arctan(double x)
{
    Angle result(atan(x));
    return result;
}

Angle Angle::arctan2(double y, double x)
{
    Angle result(atan(y/x));
    return result;
}
