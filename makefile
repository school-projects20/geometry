CC = g++
EXEC = bin/main
SRC = $(wildcard source/*.cpp)
OBJ = $(patsubst source/%.cpp, objects/%.o, $(SRC))
LDFLAGS = -lm -g -Wall -std=c++17

all : $(EXEC)

objects/%.o : source/%.cpp
	mkdir objects -p
	$(CC) -c $< -o $@ $(LDFLAGS)

$(EXEC) : $(OBJ)
	mkdir bin -p
	$(CC) -o $(EXEC) $(OBJ) $(LDFLAGS)

run : $(EXEC)
	./$(EXEC)

clean :
	rm -f $(OBJ)

rmproper :
	rm -r objects
	rm -r bin
